$(document).ready(function(){	

	$('#import_user_list_form').validate({
    rules: {
      "file": {
       required: true,
       fileFormat: true
      }
    }
	});

	$.validator.addMethod('fileFormat', function (value, element) {
	    return /([a-zA-Z0-9\s_\\.\-:])+(.csv)$/.test(value);
	}, 'Only CSV File is Allowed')

});