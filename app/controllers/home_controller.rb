class HomeController < ApplicationController
	require 'csv'
	def index
		return unless params[:file].present?
		data = []
    compare_value = Proc.new { |element| element.first == MATCHING_ID }
    csv = CSV.read(params[:file].path)
    end_range = csv.size - ONE
    csv[ZERO..end_range].each_with_index do |row, index|
      data << { row: row, rolling_percent: calculate_rolling_percent(csv, index, compare_value) }
    end
    start_and_end_index = start_and_end_indexes(data)
    @start_period = csv[start_and_end_index.first]
    @end_period = csv[start_and_end_index.last]
    @total_duration = csv[start_and_end_index.first..start_and_end_index.last].select(&compare_value).count.to_f / HOUR.to_f
	end

  private
  def calculate_rolling_percent(data, index, compare_value)
    if index >= ONE_FIFTY
      first_index = index
      second_index = index - ONE_FOURTY_NINE
      data[second_index..first_index].select(&compare_value).count.to_f / ONE_FIFTY.to_f
    end
  end

  def start_and_end_indexes(data)
  	triggered_index = ZERO
  	end_index = ZERO
  	data.each_with_index do |hash, index|
  		if hash[:rolling_percent].present? && hash[:rolling_percent] >= ROLLING_PERCENT
  			furthur_epochs = index + ONE_FIFTY
  			end_epochs = index + THREE_SEVENTY_FIVE
  			is_start = data[index..furthur_epochs].detect { |data| data[:rolling_percent] < ROLLING_PERCENT }
  			is_end = data[index..end_epochs].detect { |data| data[:rolling_percent] > ROLLING_PERCENT }
  			triggered_index = index unless is_start.present? || triggered_index > ZERO
  			unless is_end.present?
         end_index = index
         break
  			end
  		end
  	end
  	[triggered_index - ONE_FOURTY_NINE, end_index]
  end
end